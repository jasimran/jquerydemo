# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create(id:1, name:'Clothes', notes:'Can be found on aisle 4')
Category.create(id:2,name:'Food', notes:'Can be found on aisle 7')
Category.create(id:3,name:'Furniture', notes:'Can be found on aisle 2')

SalesItem.create(name:'Shirt', category_id:1, price:36.5)
SalesItem.create(name:'Gloves', category_id:1, price:6.25)
SalesItem.create(name:'Pants', category_id:1, price:10)
SalesItem.create(name:'Ice Cream', category_id:2, price:24)
SalesItem.create(name:'Oranges', category_id:2, price:48.12)